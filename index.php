<?php
require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("shaun");
$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

echo "Name : ". $sheep->name; // "shaun"
echo "<br>";
echo "legs : ". $sheep->legs; // 4
echo "<br>";
echo "cold blooded : ". $sheep->cold_blooded; // "no"
echo "<br><br>";
echo "Name : ". $kodok->name; // "shaun"
echo "<br>";
echo "legs : ". $kodok->legs; // 4
echo "<br>";
echo "cold blooded : ". $kodok->cold_blooded; // "no"
echo "<br>";
echo "Jump : ". $kodok->jump; // 4
echo "<br><br>";
echo "Name : ". $sungokong->name; // "shaun"
echo "<br>";
echo "legs : ". $sungokong->legs; // 4
echo "<br>";
echo "cold blooded : ". $sungokong->cold_blooded; // "no"
echo "<br>";
echo "Yell : ". $sungokong->yell; // 4
echo "<br><br>";


?>